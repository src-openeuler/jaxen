Name:           jaxen
Summary:        An XPath engine written in Java
Version:        1.2.0
Release:        3
License:        BSD
URL:            https://github.com/jaxen-xpath/jaxen
Source0:        %{url}/archive/v%{version}/jaxen-1.2.0.tar.gz

BuildArch:      noarch
BuildRequires:  maven-local mvn(jdom:jdom) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-assembly-plugin) mvn(xerces:xercesImpl)
BuildRequires:  mvn(xml-apis:xml-apis) mvn(dom4j:dom4j)

%description
Jaxen is an open source XPath library written in Java. It is adaptable
to many different object models and also possible to write adapters that treat non-XML trees

%package        devel
Summary:         Samples for jaxen
Requires:       jaxen = %{version}-%{release}
Provides:       jaxen-demo
Obsoletes:      jaxen-demo

%description    devel
This package provides samples files for jaxen.


%package javadoc
Summary: Javadoc for jaxen

%description javadoc
This package provide javadoc file for jaxen.


%prep
%autosetup -n %{name}-%{version} -p1

%pom_remove_plugin :maven-source-plugin
rm -rf src/java/main/org/jaxen/xom
%pom_remove_dep xom:xom

%mvn_file : jaxen

%build
%mvn_build -f


%install
%mvn_install

install -d -m 755 %{buildroot}%{_datadir}/jaxen/samples
cp -pr src/java/samples/* %{buildroot}%{_datadir}/jaxen/samples

%files -f .mfiles
%doc LICENSE.txt

%files devel
%{_datadir}/jaxen

%files javadoc -f .mfiles-javadoc
%doc LICENSE.txt

%changelog
* Tue Jan 21 2020 duyeyu <dueyu@huawei.com> - 1.2.0-3
- update to 1.2.0-3

* Sat Dec 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.6-19
- Package init
